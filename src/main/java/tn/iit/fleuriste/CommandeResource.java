package tn.iit.fleuriste;

import tn.iit.fleuriste.domain.Commande;
import tn.iit.fleuriste.domain.Fleur;
import tn.iit.fleuriste.domain.Order;
import tn.iit.fleuriste.domain.Panier;
import tn.iit.fleuriste.repository.ClientBD;
import tn.iit.fleuriste.repository.CommandeBD;
import tn.iit.fleuriste.repository.FleurBD;
import tn.iit.fleuriste.repository.PanierBD;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

@Path("/commandes")
public class CommandeResource {

    @GET
    public List<Commande> getAll() {
        return CommandeBD.getCommandes ();
    }


    @POST
    public void add(
            Commande commande
    ) {
        CommandeBD.getCommandes ().add (
                new Commande (
                        commande.getIdClient (),
                        commande.getOrders ()));
        Panier panier = PanierBD.getOne (commande.getIdClient ());
        if (panier != null)
        {
            panier.setOrders (new ArrayList<> ());
            for (Order order:commande.getOrders ())
            {
                for (Fleur fleur:FleurBD.getFleurs ())
                {
                    if(order.fleur.getCode ()==fleur.getCode ())
                    {
                        fleur.setQuantite (fleur.getQuantite () - order.getQuantite ());
                        break;
                    }
                }
            }
        }
    }


    @DELETE
    @Path("{code}")
    public boolean delete(@PathParam("code") int code) {
        if (CommandeBD.getOne (code) != null) {
            CommandeBD.getCommandes ().remove (CommandeBD.getOne (code));
            return true;
        }
        return false;
    }
}
