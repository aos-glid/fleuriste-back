package tn.iit.fleuriste.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Commande {

    private int code;
    private Date date;
    private int idClient;
    private List<Order> orders= new ArrayList<> ();
    private static int compteur=0;

    public Commande() {
    }

    public Commande(int idClient,List<Order> orders) {
        this.code=++compteur;
        this.date = new Date ();
        this.idClient = idClient;
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "code=" + code +
                ", date=" + date +
                ", idClient=" + idClient +
                ", orders=" + orders +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass () != o.getClass ()) return false;
        Commande commande = (Commande) o;
        return code == commande.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash (code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
