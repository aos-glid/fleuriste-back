package tn.iit.fleuriste.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

public class Order {
    public Fleur fleur;
    public int quantite;

    public Order() {
    }

    public Order(Fleur fleur, int quantite) {
        this.fleur = fleur;
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Order{" +
                ", fleur=" + fleur +
                ", quantite=" + quantite +
                '}';
    }

    public Fleur getFleur() {
        return fleur;
    }

    public void setFleur(Fleur fleur) {
        this.fleur = fleur;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
