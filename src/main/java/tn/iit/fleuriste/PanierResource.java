package tn.iit.fleuriste;

import tn.iit.fleuriste.domain.Order;
import tn.iit.fleuriste.domain.Panier;
import tn.iit.fleuriste.repository.PanierBD;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Path ("/paniers")
public class PanierResource {

    @GET
    public List<Panier> getAll(){
        return PanierBD.getPaniers ();
    }

    @GET
    @Path ("{clientId}")
    public Panier getCart(@PathParam ("clientId") int clientId){
        return PanierBD.getOne (clientId);
    }

    @POST
    @Path ("{clientId}")
    public void addToCart(@PathParam ("clientId") int clientId,List<Order> orders){
        PanierBD.addToCart(clientId,orders);
    }

}
