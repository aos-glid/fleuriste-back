package tn.iit.fleuriste.domain;

import java.util.ArrayList;
import java.util.List;

public class Panier {

    private int clientId;
    private List<Order> orders=new ArrayList<> ();

    public Panier() {
    }

    public Panier(int clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "Panier{" +
                "clientId=" + clientId +
                ", orders=" + orders +
                '}';
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
