package tn.iit.fleuriste.repository;

import tn.iit.fleuriste.domain.Order;
import tn.iit.fleuriste.domain.Panier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PanierBD {
    private static List<Panier> paniers = new ArrayList<> ();

    static {

        paniers.add (new Panier (1));

    }

    public static List<Panier> getPaniers() {
        return paniers;
    }

    public static Panier getOne(int code) {
        for (Panier fleur : paniers) {
            if (fleur.getClientId () == code) {
                return fleur;
            }
        }
        return null;
    }

    public static void addToCart(int clientId, List<Order> orders) {
        Panier panier = getOne (clientId);
        if (panier != null) {
            List<Order> panierOrders = panier.getOrders ();
            for (Order order : orders) {
                boolean exist = false;
                for (Order currentOrder : panierOrders) {
                    if (order.fleur.getCode () == currentOrder.fleur.getCode ()) {
                        currentOrder.setQuantite (currentOrder.getQuantite () + 1);
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    panier.getOrders ().add (order);
                }
            }
        }
    }
}
