package tn.iit.fleuriste.repository;

import tn.iit.fleuriste.domain.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientBD {
    private static List<Client> clients = new ArrayList<> ();
    static {

        clients.add(new Client ("troliste", "1996medmsd", "Mohamed","Mseddi"));


    }
    public static List<Client> getClients() {
        return clients;
    }

    public static Client getOne(int id){
        for (Client client : clients) {
            if (client.getId () == id) {
                return client;
            }
        }
        return null;
    }
    public static Client verify(String login,String password){
        for (Client client : clients) {
            if (client.getLogin ().equals (login) && client.getPassword ().equals (password)) {
                return client;
            }
        }
        return null;
    }
}
