package tn.iit.fleuriste;

import tn.iit.fleuriste.domain.Fleur;
import tn.iit.fleuriste.repository.FleurBD;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/fleurs")
public class FleureResource {
    @GET
    public List<Fleur> getAll() {
        return FleurBD.getFleurs ();
    }

    @GET
    @Path("{code}")
    public Fleur getOne(@PathParam("code") int code) {
        return FleurBD.getOne (code);
    }

    @POST
    public void add(
            Fleur fleur
    ) {
        FleurBD.getFleurs ().add (new Fleur (fleur.getLibelle (), fleur.getCategorie (), fleur.getCouleur (), fleur.getQuantite ()));
    }

    @PUT
    public void update(Fleur fleur) {

        Fleur fleurToUpdate = FleurBD.getOne (fleur.getCode ());
        if (fleurToUpdate != null) {
            fleurToUpdate.setCategorie (fleur.getCategorie ());
            fleurToUpdate.setCouleur (fleur.getCouleur ());
            fleurToUpdate.setLibelle (fleur.getLibelle ());
            fleurToUpdate.setQuantite (fleur.getQuantite ());
        }
    }

    @DELETE
    @Path("{code}")
    public boolean delete(@PathParam("code") int code) {
        if (FleurBD.getOne (code) != null) {
            FleurBD.getFleurs ().remove (FleurBD.getOne (code));
            return true;
        }
        return false;
    }

}
