package tn.iit.fleuriste.repository;

import tn.iit.fleuriste.domain.Fleur;

import java.util.ArrayList;
import java.util.List;

public class FleurBD {
    private static List<Fleur> fleurs = new ArrayList<> ();
    static {

        fleurs.add(new Fleur ("Primula", "Primulaceae", "White",10));

        fleurs.add(new Fleur ("Rosa", "Rosaceae", "Pink",50));

        fleurs.add(new Fleur ("Papaver", "Papaveraceae", "Orange",12));

    }
    public static List<Fleur> getFleurs() {
        return fleurs;
    }

    public static Fleur getOne(int code){
        for (Fleur fleur : fleurs) {
            if (fleur.getCode () == code) {
                return fleur;
            }
        }
        return null;
    }
}
