package tn.iit.fleuriste.repository;

import tn.iit.fleuriste.domain.Commande;

import java.util.ArrayList;
import java.util.List;

public class CommandeBD {
    private static List<Commande> commandes = new ArrayList<> ();

    public static List<Commande> getCommandes() {
        return commandes;
    }

    public static Commande getOne(int code){
        for (Commande commande : commandes) {
            if (commande.getCode () == code) {
                return commande;
            }
        }
        return null;
    }
}
