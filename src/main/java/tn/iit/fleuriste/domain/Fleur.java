package tn.iit.fleuriste.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

//@XmlRootElement(name = "Fleur")
public class Fleur {
    private int code;
    private String libelle;
    private String categorie;
    private String couleur;
    private int quantite;
    private static int compteur=0;

    public Fleur(){

    }
    public Fleur(String libelle, String categorie, String couleur, int quantite) {
        this.code = ++compteur;
        this.libelle = libelle;
        this.categorie = categorie;
        this.couleur = couleur;
        this.quantite = quantite;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Fleur{" +
                "code=" + code +
                ", libelle='" + libelle + '\'' +
                ", categorie='" + categorie + '\'' +
                ", couleur='" + couleur + '\'' +
                ", quantite=" + quantite +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass () != o.getClass ()) return false;
        Fleur fleur = (Fleur) o;
        return code == fleur.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash (code);
    }
}
