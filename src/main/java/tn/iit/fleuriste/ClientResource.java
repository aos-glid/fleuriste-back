package tn.iit.fleuriste;

import tn.iit.fleuriste.domain.Client;
import tn.iit.fleuriste.domain.ClientDto;
import tn.iit.fleuriste.repository.ClientBD;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/clients")
public class ClientResource {
    @GET
    public List<Client> getAll() {
        return ClientBD.getClients ();
    }

    @GET
    @Path ("{id}")
    public Client get(@PathParam ("id") int id){
        return ClientBD.getOne (id);
    }

    @POST
    @Path("login")
    public Client login(ClientDto clientDto) {
        return ClientBD.verify (clientDto.getLogin (), clientDto.getPassword ());
    }


    @POST
    public void register(
            Client client
    ) {
        ClientBD.getClients ().add (
                new Client (
                        client.getLogin (),
                        client.getPassword (),
                        client.getFirstName (),
                        client.getLastName ()
                ));
    }
}
